<?php
require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();

$channel->queue_declare('messages-list', false, false, false, false);

echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";


$callback = function($msg) {
    $data = (array) json_decode($msg->body);
    sendSms($data['to'],$data['message']);    

};

$channel->basic_consume('messages-list','', false, true, false, false, $callback);

while(count($channel->callbacks)) {
    $channel->wait();
}

function sendSms($to, $message) {
  sleep(rand(3, 5));
  return true;
}

