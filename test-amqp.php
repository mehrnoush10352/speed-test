<?php
ini_set('dispaly_errors',1);
$connection = new AMQPConnection( array(
    'host'          => $this->con_params['localhost'],
    'port'          => $this->con_params['5672'],
    'login'         => $this->con_params['guest'],
    'password'      => $this->con_params['guest'],
    'read_timeout'  => 1,//seconds
    'write_timeout' => 1,//seconds
    'connect_timeout' => 1,//seconds
)
);
$connection->pconnect();
$channel = new AMQPChannel($connection);
$xchange = new AMQPExchange($channel);
try{
  $routing_key = 'hello';
  $queue = new AMQPQueue($channel);
  $queue->setName($routing_key);
  $queue->setFlags(AMQP_NOPARAM);
  $queue->declare();
  $message = 'howdy-do';
  $xchange->publish($message, $routing_key);
}catch(Exception $x){
  print_r($x);
}
