#!/usr/local/bin/php -q
<?php

$address = '0.0.0.0';
$port = 55551;

if (($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
    echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
}

if (socket_bind($sock, $address, $port) === false) {
    echo "socket_bind() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";
}

if (socket_listen($sock, 5) === false) {
    echo "socket_listen() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";
}
$enable = true;
do {
    if (($msgsock = socket_accept($sock)) === false) {
        echo "socket_accept() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";
        break;
    }
    if ($enable) {
        $msg = "OK";
        socket_write($msgsock, $msg, strlen($msg));

        /* Send instructions. */
        if (false === ($request = socket_read($msgsock, 2048, PHP_NORMAL_READ))) {
            echo "socket_read() failed: reason: " . socket_strerror(socket_last_error($msgsock)) . "\n";
        }
        if($enable) {
            if (!empty(trim($request))) { 
                $enable = false;
                $params = array();
                $explodeHttp = explode(' ', $request);
                if (strpos($explodeHttp[1],'/') !== false) {
                    $explodePath = explode('/', $explodeHttp[1]);
                    if(strpos(end($explodePath),'?') !== false) {
                        $paramsString =  str_replace('?', '', end($explodePath));
                        $paramString = explode('&',$paramsString);
                        foreach($paramString as $paramString) {
                            $export = explode('=',$paramString); 
                            $params[$export[0]] = $export[1];  
                        }
                    }
                }
                if(count($params) > 0) {
                    $redis = new Redis(); 
                    $redis->connect('127.0.0.1', 6379);

                    if (isset($params['to']) && isset($params['message'])) {
                        $message = array('to' => $params['to'], 'message' =>  $params['message']);
                        $redis->rpush('messages-list',json_encode($message));
                    } 
                }

                $response = '
                    HTTP/1.0 200 OK
                    Date: Fri, 31 Dec 1999 23:59:59 GMT
                    Content-Type: text/html
                    Content-Length:170

                    <html><head><title>Welcome to Amazing Site!<title></head>
                    <body><p>This is under construction. Please come back later.sorry!</p></body>
            <html>
';
                socket_write($msgsock, $response, strlen($response));        

            }
        }
        $enable = true;
        socket_close($msgsock);
    } 

} while (true);
socket_close($sock);
?>
