<?php

$address = '0.0.0.0';
$port = 5555;

if (($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
    echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
}

if (socket_bind($sock, $address, $port) === false) {
    echo "socket_bind() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";
}

if (socket_listen($sock, 5) === false) {
    echo "socket_listen() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";
}
do {
if (($msgsock = socket_accept($sock)) === false) {
    echo "socket_accept() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";
}

if (false === ($request = socket_read($msgsock, 2048, PHP_NORMAL_READ))) {
    echo "socket_read() failed: reason: " . socket_strerror(socket_last_error($msgsock)) . "\n";
}

$available = false;
for($i = 1; $i < 4; $i++) {
    $conAddress = '0.0.0.0';
    $conPort = '5555'.$i;
    if (($con = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
            echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
    }

    if (socket_bind($con, $conAddress, $conPort) === false) {
            echo "socket_bind() failed: reason: " . socket_strerror(socket_last_error($con)) . "\n";
    }

    if (socket_listen($con, 5) === false) {
            echo "socket_listen() failed: reason: " . socket_strerror(socket_last_error($con)) . "\n";
    }

    if (($msgsock = socket_accept($con)) === false) {
            echo "socket_accept() failed: reason: " . socket_strerror(socket_last_error($con)) . "\n";
                break;
    }
    $msg = 'status';
    /* Send instructions. */
    socket_write($msgsock, $msg, strlen($msg));

    if (false === ($status = socket_read($msgsock, 2048, PHP_NORMAL_READ))) {
        echo "socket_read() failed: reason: " . socket_strerror(socket_last_error($msgsock)) . "\n";
    }

    if ($status == 'OK') {
        $available = true;
        socket_write($msgsock, $request, strlen($request));

        if (false === ($response = socket_read($msgsock, 2048, PHP_NORMAL_READ))) {
            echo "socket_read() failed: reason: " . socket_strerror(socket_last_error($msgsock)) . "\n";
        }
        
    }
    

    socket_close($msgsock);
    if ($available) {
        break;
    }
}
socket_write($msgsock, $response, strlen($response));
socket_close($msgsock);
} while (true);
socket_close($sock);
?>
