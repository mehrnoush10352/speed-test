<?php
require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();
$channel->queue_declare('messages-list', false, false, false, false);
$i = 1;
if (isset($_GET['to']) && isset($_GET['message'])) {

    $message = json_encode(array('to' => $_GET['to'] . $i, 'message' =>  $_GET['message'] . $i));
    $msg = new AMQPMessage($message);
    $channel->basic_publish($msg, '', 'messages-list');
    $i++;

}

$channel->close();
$connection->close();

